# chatWeMaintain
Test technique **WeMaintain** : Application **Ionic** d'envoie de message connectée au service **SendBird**.
## Installation
- Au préalable, il est nécessaire d'avoir installer `Ionic CLI` et `npm`.
- Installation des dépendances `npm` :
`npm i`

## Lancement du projet
- Depuis un terminal de commande :
`ionic serve`

Le projet est ensuite accessible à l'adresse : http://localhost:8100

## Utilisation
### Page de connexion
Inscrire un nom d'utilisateur. Si celui n'existe pas, il sera automatiquement créé.
### Page principale
Navigation entre 2 pages : Chat et Utilisateurs connectés. Un troisième bouton "Déconnexion" permet de se déconnecter du service **SendBird**.

- Page "Chat" : Un unique channel où tous les messages sont rassemblés.
- Page "Connectés" : Liste des utilisateurs actuellement connectés.
