export interface User {
    userId: string;
    nickname: string;
    profile_url: string;
    access_token: string;
    is_active: boolean;
    is_online: boolean;
    last_seen_at: number;
}
