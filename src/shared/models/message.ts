import { User } from './user';

export interface Message {
    message: string;
    messageId: number,
    createdAt: number,
    _sender: User;
}
