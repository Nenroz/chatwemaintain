import { Injectable } from '@angular/core';
import * as SendBird from '../../../node_modules/sendbird/Sendbird.min.js';
import { User } from '../models/user';

@Injectable()
export class SendbirdService {

    sb: any;

    constructor() {
        this.sb = new SendBird({
            appId: '3A263EF9-ADA3-4006-91CD-07A7536D80B9'
        });
    }

    connect(user: User, callback: any): void {
        this.sb.connect(user.userId, (user, error) => callback(user, error));
    }

    disconnect(callback: any): void {
        this.sb.disconnect(callback());
    }

    getChannel(channelUrl: string, callback: any): void {
        this.sb.OpenChannel.getChannel(channelUrl, (channel, error) => {
            callback(channel, error)
        });
    }

    enterChannel(channel: any, callback: any): void {
        channel.enter((response, error) => {
            callback(response, error);
        });
    }

    usersChannel(channel: any, callback: any): void {
        let participantListQuery = channel.createParticipantListQuery();

        participantListQuery.next((users, error) => {
            callback(users, error);
        });
    }

    sendMessage(channel: any, message: string, callback: any): void {
        channel.sendUserMessage(message, '', '', (sentMessage, error) => {
            callback(sentMessage, error);
        });
    }

    previousMessages(channel: any, callback: any): void {
        let messageListQuery = channel.createPreviousMessageListQuery();

        messageListQuery.load(25, false, (messages, error) => {
            callback(messages, error);
        });
    }

}
