import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ChatPage } from '../pages/chat/chat';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { IonicStorageModule } from '@ionic/storage';
import { Config } from '../config';
import { HttpClientModule } from '@angular/common/http';
import { ConnectedPage } from '../pages/connected/connected';
import { SendbirdService } from '../shared/services/sendbird';

@NgModule({
    declarations: [
        MyApp,
        ChatPage,
        ConnectedPage,
        TabsPage,
        LoginPage
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: '__cwm',
            driverOrder: ['sqlite', 'websql', 'indexeddb']
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        TabsPage,
        LoginPage,
        ChatPage,
        ConnectedPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Config,
        SendbirdService
    ]
})
export class AppModule {}
