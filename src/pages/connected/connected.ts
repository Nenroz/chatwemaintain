import { Component } from '@angular/core';
import { Loading, LoadingController, NavController } from 'ionic-angular';
import { User } from '../../shared/models/user';
import { SendbirdService } from '../../shared/services/sendbird';

@Component({
    selector: 'page-connected',
    templateUrl: 'connected.html'
})
export class ConnectedPage {

    users: User[];
    show = false;
    loading: Loading;
    channel: any;

    constructor(
        public navCtrl: NavController,
        private loadingCtrl: LoadingController,
        private sendbird: SendbirdService
    ) { }

    ionViewWillEnter() {
        if (this.channel) {
            this.init();
        } else {
            this.loading = this.loadingCtrl.create({ content: 'Récupération ...', duration: 10000 });
            this.loading.present();
            this.sendbird.getChannel('main_chan', (channel, error) => {
                if (error) console.log(error);
                this.channel = channel;
                this.sendbird.enterChannel(channel, (res, error) => {
                    if (error) console.log(error);
                    this.sendbird.usersChannel(this.channel, (users, error) => {
                        if (error) console.log(error);
                        this.loading.dismiss();
                        this.show  = true;
                        this.users = users;
                    });
                });
            });
        }
    }

    init() {
        this.sendbird.usersChannel(this.channel, (users, error) => {
            if (error) console.log(error);
            this.show  = true;
            this.users = users;
        });
    }
}
