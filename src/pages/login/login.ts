import { Component } from '@angular/core';
import { AlertController, Loading, LoadingController, NavController } from 'ionic-angular';
import { User } from '../../shared/models/user';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { SendbirdService } from '../../shared/services/sendbird';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})
export class LoginPage {

    user: User;
    loading: Loading;

    constructor(
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        private storage: Storage,
        private loadingCtrl: LoadingController,
        private sendbird: SendbirdService
    ) {
        this.user = { } as User;
    }

    login(user: User): void {
        this.loading = this.loadingCtrl.create({ content: 'Connexion en cours ...', duration: 10000 });
        this.loading.present();
        if (Object.keys(user).length !== 0 && user.constructor === Object && user.userId.length !== 0) {
            this.sendbird.connect(user, (usr, error) => {
                if (error) {
                    console.log(error);
                    let message = 'Une erreur est survenue lors de la connexion.';
                    this.loading.dismiss();
                    let alert = this.alertCtrl.create({
                        subTitle: message,
                        buttons: ['OK']
                    });
                    alert.present();
                } else {
                    this.storage.set('currentUser', user).then(() => {
                        this.loading.dismiss();
                        this.navCtrl.setRoot(TabsPage);
                    });
                }
            });
        } else {
            this.loading.dismiss();
            let alert = this.alertCtrl.create({
                subTitle: 'Veuillez entrer un nom d\'utilisateur.',
                buttons: ['OK']
            });
            alert.present();
        }
    }
}
