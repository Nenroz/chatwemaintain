import { Component } from '@angular/core';
import { Loading, LoadingController, NavController } from 'ionic-angular';
import { Message } from '../../shared/models/message';
import { User } from '../../shared/models/user';
import { Storage } from '@ionic/storage';
import { SendbirdService } from '../../shared/services/sendbird';

@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html'
})
export class ChatPage {

    channel: any;
    currentUser: User;
    loading: Loading;
    messages: Message[];
    message: Message;
    show = false;
    isSending = false;

    constructor(
        public navCtrl: NavController,
        private loadingCtrl: LoadingController,
        private storage: Storage,
        private sendbird: SendbirdService
    ) {
        this.message = { } as Message;
    }

    ionViewWillEnter() {
        if (this.channel) {
            this.init();
        } else {
            this.loading = this.loadingCtrl.create({ content: 'Récupération ...', duration: 10000 });
            this.loading.present();
            this.storage.get('currentUser').then(user => {
                if (user) {
                    this.currentUser = user;
                    this.sendbird.connect(user, (usr, error) => {
                        if (error) console.log(error);
                        this.sendbird.getChannel('main_chan', (channel, error) => {
                            if (error) console.log(error);
                            this.channel = channel;
                            this.sendbird.enterChannel(channel, (res, error) => {
                                if (error) console.log(error);
                                this.sendbird.previousMessages(this.channel, (messages, error) => {
                                    if (error) console.log(error);
                                    this.loading.dismiss();
                                    this.messages = messages;
                                    this.show  = true;
                                });
                            });
                        });
                    });
                }
            });
        }
    }

    init() {
        this.sendbird.previousMessages(this.channel, (messages, error) => {
            if (error) console.log(error);
            this.messages = messages;
            this.show  = true;
        });
    }

    send(message: Message) {
        this.isSending = true;
        this.sendbird.sendMessage(this.channel, message.message, (sentMessage, error) => {
            if (error) console.log(error);
            this.message = { } as Message;
            this.init();
            this.isSending = false;
        });
    }

}
