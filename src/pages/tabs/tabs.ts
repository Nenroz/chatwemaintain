import { Component } from '@angular/core';

import { ConnectedPage } from '../connected/connected';
import { ChatPage } from '../chat/chat';
import { Storage } from '@ionic/storage';
import { Loading, LoadingController, NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { SendbirdService } from '../../shared/services/sendbird';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {

    tab1Root = ChatPage;
    tab2Root = ConnectedPage;

    loading: Loading;

    constructor(
        public navCtrl: NavController,
        private storage: Storage,
        private loadingCtrl: LoadingController,
        private sendbird: SendbirdService
    ) { }

    ionViewDidLoad() {
        this.storage.get('currentUser').then(user => {
            if (user) {
                this.sendbird.connect(user, (usr, error) => {
                    if (error) console.log(error);
                    this.sendbird.getChannel('main_chan', (channel, error) => {
                        if (error) console.log(error);
                        this.sendbird.enterChannel(channel, (res, error) => {
                            if (error) console.log(error);
                        });
                    });
                });
            } else {
                this.navCtrl.setRoot(LoginPage);
            }
        });
    }

    logout() {
        this.loading = this.loadingCtrl.create({ content: 'Déconnexion ...', duration: 10000 });
        this.loading.present();
        this.sendbird.disconnect(() => {
            this.storage.remove('currentUser').then(() => {
                this.loading.dismiss();
                this.navCtrl.setRoot(LoginPage);
            });
        });
    }
}
